const express = require("express");
const controllers = require("../app/controllers");
const apiRouter = express.Router();
const swaggerUi = require("swagger-ui-express");
const yaml = require("yamljs"); // Import yaml
const swaggerDocument = yaml.load("./openapi.yaml"); // Load swagger document

/**
 * TODO: Implement your own API
 *       implementations
 */
// Router Documentation
apiRouter.get("/docs/swagger.json", controllers.api.v1.docsController.getSwagger);
apiRouter.use("/docs", swaggerUi.serve);
apiRouter.get("/docs", swaggerUi.setup(swaggerDocument));
// Router Type Users - Bhadrika Wira 5 Endpoint
apiRouter.get("/api/v1/type_users", controllers.api.v1.authController.authorize, controllers.api.v1.type_usersController.list);
apiRouter.get("/api/v1/type_user/:id", controllers.api.v1.authController.authorize, controllers.api.v1.type_usersController.show);
apiRouter.post("/api/v1/createType_users", controllers.api.v1.authController.authorize, controllers.api.v1.type_usersController.create);
apiRouter.put("/api/v1/updateType_users/:id", controllers.api.v1.authController.authorize, controllers.api.v1.type_usersController.update);
apiRouter.delete("/api/v1/deleteType_users/:id", controllers.api.v1.authController.authorize, controllers.api.v1.type_usersController.destroy);
// Router Type Cars - Rayisa 5 Endpoint
apiRouter.get("/api/v1/type_cars", controllers.api.v1.authController.authorize, controllers.api.v1.type_carsController.listTypeCar);
apiRouter.get("/api/v1/type_car/:id", controllers.api.v1.authController.authorize, controllers.api.v1.type_carsController.showTypeCar);
apiRouter.post("/api/v1/createType_cars", controllers.api.v1.authController.authorize, controllers.api.v1.type_carsController.createTypeCar);
apiRouter.put("/api/v1/updateType_cars/:id", controllers.api.v1.authController.authorize, controllers.api.v1.type_carsController.updateTypeCar);
apiRouter.delete("/api/v1/deleteType_cars/:id", controllers.api.v1.authController.authorize, controllers.api.v1.type_carsController.destroyTypeCar);
// Router Authentication & Authorization - Vito 4 Endpoint
apiRouter.post("/api/v1/login", controllers.api.v1.authController.login);
apiRouter.post("/api/v1/registerMember", controllers.api.v1.authController.registerMember);
apiRouter.post("/api/v1/registerAdmin", controllers.api.v1.authController.authorize, controllers.api.v1.authController.registerAdmin);
apiRouter.get("/api/v1/whoami", controllers.api.v1.authController.authorize, controllers.api.v1.authController.whoAmI);
// Router Users - Dalih 5 Endpoint
apiRouter.get("/api/v1/users", controllers.api.v1.authController.authorize, controllers.api.v1.usersController.list);
apiRouter.get("/api/v1/user/:id", controllers.api.v1.authController.authorize, controllers.api.v1.usersController.show);
apiRouter.put("/api/v1/users/updateToMember/:id", controllers.api.v1.authController.authorize, controllers.api.v1.usersController.updateToMember);
apiRouter.put("/api/v1/users/updateToAdmin/:id", controllers.api.v1.authController.authorize, controllers.api.v1.usersController.updateToAdmin);
apiRouter.delete("/api/v1/deleteUsers/:id", controllers.api.v1.authController.authorize, controllers.api.v1.usersController.destroy);
// Router Cars - Raffi 6 Endpoint
apiRouter.get("/api/v1/cars", controllers.api.v1.authController.authorize, controllers.api.v1.carsController.listCar);
apiRouter.get("/api/v1/car/:id", controllers.api.v1.authController.authorize, controllers.api.v1.carsController.showCar);
apiRouter.post("/api/v1/createCar", controllers.api.v1.authController.authorize, controllers.api.v1.carsController.createCar);
apiRouter.put("/api/v1/updateCar/:id", controllers.api.v1.authController.authorize, controllers.api.v1.carsController.updateCar);
apiRouter.delete("/api/v1/deleteCar/:id", controllers.api.v1.authController.authorize, controllers.api.v1.carsController.destroyCar);
apiRouter.get("/api/v1/carsDeleted", controllers.api.v1.authController.authorize, controllers.api.v1.carsController.listDeletedCars);
// Router Activities Logs - Raffi 1 Endpoint
apiRouter.get("/api/v1/activity", controllers.api.v1.authController.authorize, controllers.api.v1.carsController.listActivity);

/**
 * TODO: Delete this, this is just a demonstration of
 *       error handler
 */
apiRouter.get("/api/v1/errors", () => {
    throw new Error("The Industrial Revolution and its consequences have been a disaster for the human race.");
});

apiRouter.use(controllers.api.main.onLost);
apiRouter.use(controllers.api.main.onError);

module.exports = apiRouter;
