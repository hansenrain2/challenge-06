const carsRepository = require("../repositories/carsRepository");

module.exports = {
    create(requestBody) {
        return carsRepository.create(requestBody);
    },

    update(id, requestBody) {
        return carsRepository.update(id, requestBody);
    },

    delete(id) {
        return carsRepository.delete(id);
    },

    get(id) {
        return carsRepository.find(id);
    },

    async list() {
        try {
            const cars = await carsRepository.findAll();

            return {
                data: cars,
            };
        } catch (err) {
            throw err;
        }
    },

    async listAllDeleted(showDeleted = true) {
        try {
            const cars = await carsRepository.findAllDeleted(showDeleted);

            return {
                data: cars,
            };
        } catch (err) {
            throw err;
        }
    },

    get(id) {
        return carsRepository.find(id);
    },
};
