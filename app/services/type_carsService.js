const type_carsRepository = require("../repositories/type_carsRepository");

module.exports = {
    create(requestBody) {
        return type_carsRepository.create(requestBody);
    },

    update(id, requestBody) {
        return type_carsRepository.update(id, requestBody);
    },

    delete(id) {
        return type_carsRepository.delete(id);
    },

    get(id) {
        return type_carsRepository.find(id);
    },

    async list() {
        try {
            const type_users = await type_carsRepository.findAll();

            return {
                data: type_users,
            };
        } catch (err) {
            throw err;
        }
    },

    get(id) {
        return type_carsRepository.find(id);
    },
};
