const {Type_Users} = require("../models");

module.exports = {
    create(createArgs) {
        return Type_Users.create(createArgs);
    },

    update(id, updateArgs) {
        return Type_Users.update(updateArgs, {
            where: {
                id,
            },
        });
    },

    delete(id) {
        return Type_Users.destroy({where: {id}});
    },

    find(id) {
        return Type_Users.findByPk(id);
    },

    findAll() {
        return Type_Users.findAll();
    },
};
